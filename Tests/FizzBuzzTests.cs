﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [Test]
        public void GetOutput_NumberDivisibleBy3And5_ReturnsFizzBuzz()
        {
            //Arrange
            int x = 15;
            string expected = "FizzBuzz";
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetOutput_NumberDivisibleBy3_ReturnsFizz()
        {
            //Arrange
            int x = 9;
            string expected = "Fizz";
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetOutput_NumberDivisibleBy5_ReturnsBuzz()
        {
            //Arrange
            int x = 10;
            string expected = "Buzz";
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]

        public void GetOutput_NumberNotDivisibleBy3Or5_ReturnsNumber()
        {
            //Arrange
            int x = 7;
            string expected = "7";
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetOutput_NumberIsZero_ReturnsZero()
        {
            //Arrange
            int x = 0;
            string expected = "0";
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]

        public void GetOutput_NumberIsNegative_ReturnsNumber()
        {
            //Arrange
            int x = -5;
            string expected = "-5";
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(3, "Fizz")]
        [TestCase(4, "4")]
        [TestCase(5, "Buzz")]
        [TestCase(6, "Fizz")]
        [TestCase(7, "7")]
        [TestCase(8, "8")]
        [TestCase(9, "Fizz")]
        [TestCase(10, "Buzz")]
        [TestCase(11, "11")]
        [TestCase(12, "Fizz")]
        [TestCase(13, "13")]
        [TestCase(14, "14")]
        [TestCase(55, "Buzz")]
        [TestCase(1665, "FizzBuzz")]
        [TestCase(0, "0")]
        [TestCase(-1, "-1")]
        [TestCase(-2, "-2")]
        [TestCase(-3, "Fizz")]

        public void GetOutput_Number_ReturnsNumber(int x, string expected)
        {
            //Arrange
            FizzBuzz testObject = new FizzBuzz();

            //Act
            string actual = testObject.GetOutput(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }


    }
}
