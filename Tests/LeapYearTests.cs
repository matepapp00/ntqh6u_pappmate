﻿using BeadandoFeladat2021.MainPrograms;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeadandoFeladat2021.Tests
{
    [TestFixture]
    public class LeapYearTests
    {
        [Test]

        public void checkLeapYear_YearDivisibleBy4And100And400_ReturnsTrue()
        {
            //Arrange
            int x = 2000;
            bool expected = true;
            LeapYear testObject = new LeapYear();

            //Act
            bool actual = testObject.checkLeapYear(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(2000, true)]
        [TestCase(2001, false)]
        [TestCase(2002, false)]
        [TestCase(2003, false)]
        [TestCase(2004, true)]
        [TestCase(2005, false)]
        [TestCase(2006, false)]
        [TestCase(2007, false)]
        [TestCase(2008, true)]

        public void checkLeapYear_YearDivisibleBy4And100And400_ReturnsTrue(int x, bool expected)
        {
            //Arrange
            LeapYear testObject = new LeapYear();

            //Act
            bool actual = testObject.checkLeapYear(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]

        public void checkLeapYear_YearDivisibleBy4And100_ReturnsFalse()
        {
            //Arrange
            int x = 2100;
            bool expected = false;
            LeapYear testObject = new LeapYear();

            //Act
            bool actual = testObject.checkLeapYear(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]

        public void checkLeapYear_YearDivisibleBy4_ReturnsTrue()
        {
            //Arrange
            int x = 2004;
            bool expected = true;
            LeapYear testObject = new LeapYear();

            //Act
            bool actual = testObject.checkLeapYear(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]

        public void checkLeapYear_YearNotDivisibleBy4_ReturnsFalse()
        {
            //Arrange
            int x = 2003;
            bool expected = false;
            LeapYear testObject = new LeapYear();

            //Act
            bool actual = testObject.checkLeapYear(x);

            //Assert
            Assert.AreEqual(expected, actual);
        }





    }
}
